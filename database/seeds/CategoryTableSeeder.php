<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Factory::create();
        Category::truncate();
        /*
        Category::create([
                'id'=>3,
                'name_ru'=>"Одежда",
                'name_kz'=>"Киім",
                'parent_id'=>0,
                'icon'=>"clothes.png",
                'order'=>1
            ]);
        Category::create([
                'id'=>23,
                'name_ru'=>"Мужская одежда",
                'name_kz'=>"Ерлер киімі",
                'parent_id'=>3,
                'icon'=>"clothes.png",
                'order'=>1
            ]);
        Category::create([
                'id'=>24,
                'name_ru'=>"Женская одежда",
                'name_kz'=>"Әйелдер киімі",
                'parent_id'=>3,
                'icon'=>"clothes.png",
                'order'=>2
            ]);
        Category::create([
            'id'=>4,
            'name_ru'=>"Обувь",
            'name_kz'=>"Аяқ киім",
            'parent_id'=>0,
            'icon'=>"shoes.png",
            'order'=>2
        ]);
        Category::create([
                'id'=>25,
                'name_ru'=>"Мужская обувь",
                'name_kz'=>"Ерлер аяқ киімі",
                'parent_id'=>4,
                'icon'=>"shoes.png",
                'order'=>1
            ]);
        Category::create([
                'id'=>26,
                'name_ru'=>"Женская обувь",
                'name_kz'=>"Әйелдер аяқ киімі",
                'parent_id'=>4,
                'icon'=>"shoes.png",
                'order'=>2
            ]);
        Category::create([
            'id'=>5,
            'name_ru'=>"Аксессуары",
            'name_kz'=>"Аксессуарлар",
            'parent_id'=>0,
            'icon'=>"accessories.png",
            'order'=>3
        ]);
        Category::create([
                'id'=>27,
                'name_ru'=>"Для мужчин",
                'name_kz'=>"Ерлерге",
                'parent_id'=>5,
                'icon'=>"accessories.png",
                'order'=>1
            ]);
        Category::create([
                'id'=>28,
                'name_ru'=>"Для женщин",
                'name_kz'=>"Әйелдерге",
                'parent_id'=>5,
                'icon'=>"accessories.png",
                'order'=>2
            ]);
        Category::create([
                'id'=>29,
                'name_ru'=>"Для авто",
                'name_kz'=>"Автокөліктерге",
                'parent_id'=>5,
                'icon'=>"accessories.png",
                'order'=>3
            ]);
        Category::create([
                'id'=>30,
                'name_ru'=>"Для смартфонов",
                'name_kz'=>"Смартфондарға",
                'parent_id'=>5,
                'icon'=>"accessories.png",
                'order'=>4
            ]);
        Category::create([
                'id'=>31,
                'name_ru'=>"Для компьютерной техники",
                'name_kz'=>"Компьютерлерге",
                'parent_id'=>5,
                'icon'=>"accessories.png",
                'order'=>5
            ]);

        Category::create([
            'id'=>6,
            'name_ru'=>"Все для детей",
            'name_kz'=>"Барлығы балаларға",
            'parent_id'=>0,
            'icon'=>"child.png",
            'order'=>4
        ]);
        Category::create([
            'id'=>7,
            'name_ru'=>"Спорттовары",
            'name_kz'=>"Спорт тауарлары",
            'parent_id'=>0,
            'icon'=>"sports.png",
            'order'=>5
        ]);
        Category::create([
            'id'=>8,
            'name_ru'=>"Электроника",
            'name_kz'=>"Электроника",
            'parent_id'=>0,
            'icon'=>"electronics.png",
            'order'=>6
        ]);
        
        Category::create([
            'id'=>9,
            'name_ru'=>"Автозапчасти",
            'name_kz'=>"Автокөлік саймандары",
            'parent_id'=>0,
            'icon'=>"car_parts_new.png",
            'order'=>7
        ]);
        
        Category::create([
            'id'=>43,
            'name_ru'=>"Легковые",
            'name_kz'=>"Жеңіл автокөлік",
            'parent_id'=>9,
            'icon'=>"car_parts_new.png",
            'order'=>1
        ]);

        Category::create([
            'id'=>32,
            'name_ru'=>"Грузовые",
            'name_kz'=>"Жүк көлігі",
            'parent_id'=>9,
            'icon'=>"car_parts_new.png",
            'order'=>2
        ]);

        

        Category::create([
            'id'=>10,
            'name_ru'=>"Спецтехника",
            'name_kz'=>"Арнайы техника",
            'parent_id'=>0,
            'icon'=>"spectechnics.png",
            'order'=>9
            
        ]);
        Category::create([
            'id'=>11,
            'name_ru'=>"Мебель",
            'name_kz'=>"Жиһаз",
            'parent_id'=>0,
            'icon'=>"furniture.png",
            'order'=>10
        ]);
        Category::create([
            'id'=>12,
            'name_ru'=>"Стройматериалы",
            'name_kz'=>"Құрылыс материалдары",
            'parent_id'=>0,
            'icon'=>"build_materials.png",
            'order'=>11
        ]);
        Category::create([
            'id'=>14,
            'name_ru'=>"Фастфуд",
            'name_kz'=>"Фастфуд",
            'parent_id'=>0,
            'icon'=>"fastfood.png",
            'order'=>15
        ]);
        Category::create([
            'id'=>15,
            'name_ru'=>"Кондитерские товары",
            'name_kz'=>"Кондитерлік өнімдер",
            'parent_id'=>0,
            'icon'=>"confectionery.png",
            'order'=>16
        ]);
        Category::create([
            'id'=>16,
            'name_ru'=>"Авторазбор",
            'name_kz'=>"Қолданылған көлік саймандар",
            'parent_id'=>0,
            'icon'=>"car_parts_old.png",
            'order'=>8
        ]);

        Category::create([
            'id'=>33,
            'name_ru'=>"Иномарка",
            'name_kz'=>"Иномарка",
            'parent_id'=>16,
            'icon'=>"car_parts_old.png",
            'order'=>1
        ]);

        Category::create([
            'id'=>34,
            'name_ru'=>"Ваз",
            'name_kz'=>"Ваз",
            'parent_id'=>16,
            'icon'=>"car_parts_old.png",
            'order'=>2
        ]);

        

        Category::create([
            'id'=>17,
            'name_ru'=>"Товары для дома",
            'name_kz'=>"Үй керек-жарақтары",
            'parent_id'=>0,
            'icon'=>"households.png",
            'order'=>12
        ]);
        Category::create([
            'id'=>18,
            'name_ru'=>"Канцелярские товары",
            'name_kz'=>"Кеңсе тауарлары",
            'parent_id'=>0,
            'icon'=>"stationery.png",
            'order'=>13
        ]);
        Category::create([
            'id'=>19,
            'name_ru'=>"Бытовая химия",
            'name_kz'=>"Тұрмыстық химия",
            'parent_id'=>0,
            'icon'=>"housekeeping.png",
            'order'=>14
        ]);
        Category::create([
            'id'=>22,
            'name_ru'=>"Услуги",
            'name_kz'=>"Қызмет түрлері",
            'parent_id'=>0,
            'icon'=>"services.png",
            'order'=>17
        ]);

        Category::create([
            'id'=>35,
            'name_ru'=>"Бизнес",
            'name_kz'=>"Бизнес",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>1
        ]);
        
        Category::create([
            'id'=>36,
            'name_ru'=>"Авто",
            'name_kz'=>"Көлік",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>2
        ]);
        
        Category::create([
            'id'=>37,
            'name_ru'=>"Бытовые",
            'name_kz'=>"Тұрмыстық",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>3
        ]);

        Category::create([
            'id'=>38,
            'name_ru'=>"Ремонт и строительство",
            'name_kz'=>"Құрылыс-жөндеу",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>4
        ]);

        Category::create([
            'id'=>39,
            'name_ru'=>"Ремонт техники",
            'name_kz'=>"Техника жөндеу",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>5
        ]);

        Category::create([
            'id'=>40,
            'name_ru'=>"Обучение и курсы",
            'name_kz'=>"Оқуға дайындау курстары",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>6
        ]);
        
        Category::create([
            'id'=>41,
            'name_ru'=>"Уборка",
            'name_kz'=>"Жинау-тазалау",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>7
        ]);
        
        Category::create([
            'id'=>42,
            'name_ru'=>"Прочие",
            'name_kz'=>"Әртүрлі",
            'parent_id'=>22,
            'icon'=>"services.png",
            'order'=>8
        ]);
        */
        Category::create([
            'id'=>1,
            'name_ru'=>"Одежда / обувь",
            'name_kz'=>"Киім / аяқ киім",
            'parent_id'=>0,
            'icon'=>"clothes.png",
            'order'=>1
        ]);
        Category::create([
            'id'=>2,
            'name_ru'=>"Женская одежда",
            'name_kz'=>"Әйелдер киімі",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>1
        ]);
        Category::create([
            'id'=>3,
            'name_ru'=>"Женская обувь",
            'name_kz'=>"Әйелдер аяқ киімі",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>2
        ]);
        Category::create([
            'id'=>4,
            'name_ru'=>"Женское белье",
            'name_kz'=>"Әйелдер іш-киімі",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>5,
            'name_ru'=>"Одежда для беременных",
            'name_kz'=>"Жүкті әйелдер киімі",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>4
        ]);
        
        Category::create([
            'id'=>6,
            'name_ru'=>"Мужская одежда",
            'name_kz'=>"Ерлер киімі",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>5
        ]);

        Category::create([
            'id'=>7,
            'name_ru'=>"Мужская обувь",
            'name_kz'=>"Ерлер аяқ киімі",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>6
        ]);

        Category::create([
            'id'=>8,
            'name_ru'=>"Мужское белье",
            'name_kz'=>"Ерлер іш киімі",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>7
        ]);
        
        Category::create([
            'id'=>9,
            'name_ru'=>"Головные уборы",
            'name_kz'=>"Бас киімдер",
            'parent_id'=>1,
            'icon'=>"",
            'order'=>8
        ]);

        Category::create([
            'id'=>10,
            'name_ru'=>"Аксессуары",
            'name_kz'=>"Аксессуарлар",
            'parent_id'=>0,
            'icon'=>"accessories.png",
            'order'=>4
        ]);
        
         Category::create([
            'id'=>11,
            'name_ru'=>"Для мужчин",
            'name_kz'=>"Ерлерге",
            'parent_id'=>10,
            'icon'=>"",
            'order'=>1
        ]);
        
        Category::create([
            'id'=>12,
            'name_ru'=>"Для женщин",
            'name_kz'=>"Әйелдерге",
            'parent_id'=>10,
            'icon'=>"",
            'order'=>2
        ]);
        
        Category::create([
            'id'=>13,
            'name_ru'=>"Для телефонов",
            'name_kz'=>"Телефонға",
            'parent_id'=>10,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>14,
            'name_ru'=>"Для электроники",
            'name_kz'=>"Электроникаға",
            'parent_id'=>10,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>15,
            'name_ru'=>"Ювелирные изделия",
            'name_kz'=>"Зергерлік бұйымдар",
            'parent_id'=>10,
            'icon'=>"",
            'order'=>6
        ]);

        Category::create([
            'id'=>16,
            'name_ru'=>"Наручные часы",
            'name_kz'=>"Сағаттар",
            'parent_id'=>10,
            'icon'=>"",
            'order'=>7
        ]);

        Category::create([
            'id'=>17,
            'name_ru'=>"Все для детей",
            'name_kz'=>"Балаларға",
            'parent_id'=>0,
            'icon'=>"child.png",
            'order'=>2
        ]);

        Category::create([
            'id'=>18,
            'name_ru'=>"Одежда",
            'name_kz'=>"Киім",
            'parent_id'=>17,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>19,
            'name_ru'=>"Обувь",
            'name_kz'=>"Аяқ киім",
            'parent_id'=>17,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>20,
            'name_ru'=>"Мебель",
            'name_kz'=>"Жиһаз",
            'parent_id'=>17,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>21,
            'name_ru'=>"Игрушки",
            'name_kz'=>"Ойыншықтар",
            'parent_id'=>17,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>22,
            'name_ru'=>"Для школьников",
            'name_kz'=>"Мектеп оқушыларына",
            'parent_id'=>17,
            'icon'=>"",
            'order'=>5
        ]);

        Category::create([
            'id'=>23,
            'name_ru'=>"Дом / офис",
            'name_kz'=>"Үй / офис",
            'parent_id'=>0,
            'icon'=>"house_ofice.png",
            'order'=>3
        ]);

        Category::create([
            'id'=>24,
            'name_ru'=>"Товары для дома",
            'name_kz'=>"Үйге арналған тауарлар",
            'parent_id'=>23,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>25,
            'name_ru'=>"Мебель",
            'name_kz'=>"Жиһаз",
            'parent_id'=>23,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>26,
            'name_ru'=>"Для дома",
            'name_kz'=>"Үйге арналған",
            'parent_id'=>25,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>27,
            'name_ru'=>"Посуда",
            'name_kz'=>"Ыдыс-аяқ",
            'parent_id'=>23,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>28,
            'name_ru'=>"Бытовая химия",
            'name_kz'=>"Тұрмыстық химия",
            'parent_id'=>23,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>29,
            'name_ru'=>"Канцелярские товары",
            'name_kz'=>"Кеңсе тауарлары",
            'parent_id'=>23,
            'icon'=>"",
            'order'=>5
        ]);

        Category::create([
            'id'=>76,
            'name_ru'=>"Для офиса",
            'name_kz'=>"Кеңсеге арналған",
            'parent_id'=>25,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>77,
            'name_ru'=>"Спорттовары",
            'name_kz'=>"Спорт тауар",
            'parent_id'=>0,
            'icon'=>"sports.png",
            'order'=>8
        ]);

        Category::create([
            'id'=>30,
            'name_ru'=>"Одежда",
            'name_kz'=>"Киім",
            'parent_id'=>77,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>31,
            'name_ru'=>"Обувь",
            'name_kz'=>"Аяқ киім",
            'parent_id'=>77,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>32,
            'name_ru'=>"Вело",
            'name_kz'=>"Вело",
            'parent_id'=>77,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>33,
            'name_ru'=>"Охота / рыбалка",
            'name_kz'=>"Аңшылық / балық аулау",
            'parent_id'=>77,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>34,
            'name_ru'=>"Прочие",
            'name_kz'=>"Әртүрлі",
            'parent_id'=>77,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>35,
            'name_ru'=>"Электроника",
            'name_kz'=>"Электроника",
            'parent_id'=>0,
            'icon'=>"electronics.png",
            'order'=>5
        ]);

        Category::create([
            'id'=>36,
            'name_ru'=>"Телефоны / планшеты",
            'name_kz'=>"Телефон / планшет",
            'parent_id'=>35,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>37,
            'name_ru'=>"Ноутбук / компьютер",
            'name_kz'=>"Ноутбук / компьютер",
            'parent_id'=>35,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>38,
            'name_ru'=>"Техника для дома",
            'name_kz'=>"Үйге арналған техника",
            'parent_id'=>35,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>39,
            'name_ru'=>"Техника для кухни",
            'name_kz'=>"Ас-үйге арналған техника",
            'parent_id'=>35,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>40,
            'name_ru'=>"Прочие",
            'name_kz'=>"Әртүрлі",
            'parent_id'=>35,
            'icon'=>"",
            'order'=>5
        ]);

        Category::create([
            'id'=>41,
            'name_ru'=>"Транспорт",
            'name_kz'=>"Транспорт",
            'parent_id'=>0,
            'icon'=>"car_parts_new.png",
            'order'=>6
        ]);

        Category::create([
            'id'=>42,
            'name_ru'=>"Автозапчасти",
            'name_kz'=>"Автобөлшектер",
            'parent_id'=>41,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>43,
            'name_ru'=>"Легковые",
            'name_kz'=>"Жеңіл автокөлік",
            'parent_id'=>42,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>44,
            'name_ru'=>"Грузовые",
            'name_kz'=>"Жүк көлік",
            'parent_id'=>42,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>45,
            'name_ru'=>"Спецтехника",
            'name_kz'=>"Арнайы техника",
            'parent_id'=>42,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>50,
            'name_ru'=>"Мотоцикл / запчасти",
            'name_kz'=>"Мотоцикл бөлшектері",
            'parent_id'=>42,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>46,
            'name_ru'=>"Авторазборы",
            'name_kz'=>"Ескі көлік саймандар",
            'parent_id'=>41,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>47,
            'name_ru'=>"Для иномарок",
            'name_kz'=>"Иномарка",
            'parent_id'=>46,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>48,
            'name_ru'=>"Для российских",
            'name_kz'=>"Ресей көлігі",
            'parent_id'=>46,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>49,
            'name_ru'=>"Мотоцикл",
            'name_kz'=>"Мотоцикл",
            'parent_id'=>41,
            'icon'=>"",
            'order'=>3
        ]);        
        
        Category::create([
            'id'=>51,
            'name_ru'=>"Шины / диски / колеса",
            'name_kz'=>"Дөңгелек / диск",
            'parent_id'=>41,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>52,
            'name_ru'=>"Спецтехника",
            'name_kz'=>"Арнайы техника",
            'parent_id'=>41,
            'icon'=>"",
            'order'=>5
        ]);
        
        Category::create([
            'id'=>53,
            'name_ru'=>"Стройматериалы",
            'name_kz'=>"Құрылыс материалы",
            'parent_id'=>0,
            'icon'=>"build_materials.png",
            'order'=>7
        ]);

        Category::create([
            'id'=>54,
            'name_ru'=>"Отделочные материалы",
            'name_kz'=>"Әрлеу материалдары",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>55,
            'name_ru'=>"Кирпич / бетон / блок",
            'name_kz'=>"Кірпіш / бетн / блок",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>56,
            'name_ru'=>"Окна / двери",
            'name_kz'=>"Терезе / есік",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>57,
            'name_ru'=>"Сантехника",
            'name_kz'=>"Сантехника",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>58,
            'name_ru'=>"Отопление",
            'name_kz'=>"Жылыту құралдары",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>5
        ]);

        Category::create([
            'id'=>59,
            'name_ru'=>"Электрика",
            'name_kz'=>"Электрика",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>6
        ]);

        Category::create([
            'id'=>60,
            'name_ru'=>"Краски и прочие",
            'name_kz'=>"Бояулар / Лактар",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>7
        ]);

        Category::create([
            'id'=>61,
            'name_ru'=>"Прочие",
            'name_kz'=>"Әртүрлі",
            'parent_id'=>53,
            'icon'=>"",
            'order'=>8
        ]);

        Category::create([
            'id'=>62,
            'name_ru'=>"Питание",
            'name_kz'=>"Тағамдар",
            'parent_id'=>0,
            'icon'=>"fastfood.png",
            'order'=>9
        ]);

        Category::create([
            'id'=>63,
            'name_ru'=>"Кафе",
            'name_kz'=>"Кафе",
            'parent_id'=>62,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>64,
            'name_ru'=>"Фастфуд",
            'name_kz'=>"Фастфуд",
            'parent_id'=>62,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>65,
            'name_ru'=>"Кондитерские изделия",
            'name_kz'=>"Кондитерлік өнімдер",
            'parent_id'=>62,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>66,
            'name_ru'=>"Мясо / молоко",
            'name_kz'=>"Ет / сүт",
            'parent_id'=>62,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>67,
            'name_ru'=>"Овощи / фрукты",
            'name_kz'=>"Жеміс / жидек",
            'parent_id'=>62,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>68,
            'name_ru'=>"Услуги",
            'name_kz'=>"Қызмет көрсету",
            'parent_id'=>0,
            'icon'=>"services.png",
            'order'=>10
        ]);

        Category::create([
            'id'=>69,
            'name_ru'=>"Бизнес",
            'name_kz'=>"Бизнес",
            'parent_id'=>68,
            'icon'=>"",
            'order'=>1
        ]);

        Category::create([
            'id'=>70,
            'name_ru'=>"Авто",
            'name_kz'=>"Авто",
            'parent_id'=>68,
            'icon'=>"",
            'order'=>2
        ]);

        Category::create([
            'id'=>71,
            'name_ru'=>"Бытовые",
            'name_kz'=>"Тұрмыстық",
            'parent_id'=>68,
            'icon'=>"",
            'order'=>3
        ]);

        Category::create([
            'id'=>72,
            'name_ru'=>"Ремонт / строительство",
            'name_kz'=>"Жөндеу / құрылыс",
            'parent_id'=>68,
            'icon'=>"",
            'order'=>4
        ]);

        Category::create([
            'id'=>73,
            'name_ru'=>"Ремонт техники",
            'name_kz'=>"Техника жөндеу",
            'parent_id'=>68,
            'icon'=>"",
            'order'=>5
        ]);

        Category::create([
            'id'=>74,
            'name_ru'=>"Обучение / курсы",
            'name_kz'=>"Оқыту / курстары",
            'parent_id'=>68,
            'icon'=>"",
            'order'=>6
        ]);

        Category::create([
            'id'=>75,
            'name_ru'=>"Прочие",
            'name_kz'=>"Әртүрлі",
            'parent_id'=>68,
            'icon'=>"",
            'order'=>7
        ]);

    }
}
