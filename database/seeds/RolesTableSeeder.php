<?php

use Illuminate\Database\Seeder;
use App\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
                'id'=>1,
                'name'=>'admin',
                'name_ru'=>"Администратор",
                'name_kz'=>"Администратор",
            ]);   

            Role::create([
                'id'=>2,
                'name'=>'operator',
                'name_ru'=>"Оператор",
                'name_kz'=>"Оператор",
            ]);   

            Role::create([
                'id'=>3,
                'name'=>'ads_admin',
                'name_ru'=>"Администратор слайдера",
                'name_kz'=>"Слайдер администраторы ",
            ]);   
    }
}
