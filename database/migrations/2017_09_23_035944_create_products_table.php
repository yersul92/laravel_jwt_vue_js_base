<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('comment');
            $table->string('cost');
            $table->integer('category_id')->unsigned();
            $table->integer('store_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('like_count')->default(0);
            $table->integer('dislike_count')->default(0);
            $table->integer('view_count')->default(0);
            $table->integer('degree')->default(0);
            $table->boolean('blocked')->default(false);
            $table->boolean('current')->default(false);
            $table->integer('product_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
