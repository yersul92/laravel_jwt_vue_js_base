<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->unique();
            $table->string('name');            
            $table->boolean('has_store')->default(false);
            $table->timestamp('payed_date')->nullable(); //->useCurrent()
            $table->timestamp('expire_date')->nullable();
            $table->integer('days_left')->default(3);
            $table->string('password');
            $table->boolean('agreed')->default(false);
            $table->timestamp('agreed_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
