<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');            
            $table->string('comment');
            $table->string('photo_path');            
            $table->integer('user_id')->unsigned();            
            $table->integer('city_id');
            $table->string('address');
            $table->string('phone');
            $table->integer('market_id')->default(0);            
            $table->integer('like_count')->default(0);
            $table->integer('dislike_count')->default(0);
            $table->integer('view_count')->default(0);
            $table->boolean('blocked')->default(false);
            $table->text('block_text')->nullable();
            $table->integer('degree')->default(0);
            $table->integer('store_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
