<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('head_kz');
            $table->string('head_ru');
            $table->text('text_kz');
            $table->text('text_ru');
            $table->string('img_url');
            $table->string('url')->nullable();
            $table->integer('store_id')->unsigned()->nullable();
            $table->boolean('publish')->default(false);
            $table->date('publish_date')->nullable();
            $table->date('expire_date')->nullable();
            $table->integer('ad_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
