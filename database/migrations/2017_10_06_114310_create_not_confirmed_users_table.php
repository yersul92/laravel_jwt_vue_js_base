<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotConfirmedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('not_confirmed_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->unique();
            $table->string('name');            
            $table->string('password');
            $table->string('confirm_code');
            $table->boolean('registered')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('not_confirmed_users');
    }
}
