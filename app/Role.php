<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\UserRole;

class Role extends Model
{
     
    public function users() {
        return $this->belongsToMany('App\User');
    }
    

}
