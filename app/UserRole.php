<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Role;
class UserRole extends Model
{
    //
    protected $fillable = [
        'user_id','role_id'
    ];

    public function data(){
        return $this->belongsTo(Role::class);
    }

}
