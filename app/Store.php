<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\StoreCategory;
use App\Product;
use App\User;
use App\StorePhone;

class Store extends Model
{

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','comment','photo_path','user_id','city_id','address','degree','phone',
    ];

    public static function initialize(){
        return [
            'description'=>'',
            'unit_price'=>'',
            'qty'=>''
        ];
    }
    public function categories(){
        return $this->hasMany(StoreCategory::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function user(){
        return $this->belongsTo(User::class)->select(array('id','name', 'phone'));
    } 
}
