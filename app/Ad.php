<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = ['name','phone','head_kz', 'head_ru', 'text_kz', 'text_ru','img_url','publish','expire_date','publish_date','store_id','url'];
}
