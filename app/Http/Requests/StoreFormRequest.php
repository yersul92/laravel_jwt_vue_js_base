<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required|numeric',
            'name' => 'required',
            'address' => 'required',
            'comment' => 'required',
            //'photo_path'=> 'required',            
            //'tmp_photo'=>'nullable'
            //'user_id'=> 'required',
            //'city_id'=> 'required',
            //'market_id'=> 'required',
            //'like_count'=> 'required',
            //'dislike_count'=> 'required',
            //'view_count'=> 'required',
            //'degree'=> 'required'
        ];
    }
}
