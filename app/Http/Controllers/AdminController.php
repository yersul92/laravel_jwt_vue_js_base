<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Store;
use App\Product;
use App\Http\Middleware\RoleManager;
use Carbon\Carbon;
class AdminController extends Controller
{
    public function users(Request $request){
        $userId = $request->user()->id;
        if($this->isUserAllowed($userId)==false){
            return response()->json([
                'data' => 403
            ]);
        }     
        $users = User::with(array('roles','store'))->where('id','!=',$userId)->paginate(30);
        return response()->json([
            'data' => $users
        ]);

    }

    public function userByText(Request $request,$text){
        $userId = $request->user()->id;
        if($this->isUserAllowed($userId)==false){
            return response()->json([
                'data' => 403
            ]);
        }     
        $users = User::with(array('roles','store'))->where('name','LIKE','%' . $text . '%')
        ->orWhere('phone','LIKE','%' . $text . '%')
        ->orWhereHas('store',function($query) use($text){
            $query->where('name','LIKE','%' . $text . '%');})->where('id','!=',$userId)->paginate(30);
        return response()->json([
            'data' => $users
        ]);
    }

    public function setUserPayed(Request $request){
        $adminUserId = $request->user()->id;
        if($this->isUserAllowed($adminUserId)==false){
            return response()->json([
                'data' => 403
            ]);
        }

        $userId = $request->json('user_id');
        $cost = $request->json('cost');
        $days = $request->json('days');
        if($cost==2000||$cost==9990||$cost==16990){
            $degree = 3;
        }else if($cost==2500||$cost==12990||$cost==22990){
            $degree = 2;
        }else if($cost==3000||$cost==15990||$cost==28990){
            $degree = 1;
        }
        //TODO : payment history
        $userData= User::where('id',$userId)->get()->first();
        Store::where('user_id',$userId)->update(['degree'=>$degree]);
        Product::where('user_id',$userId)->update(['degree'=>$degree]);
        $expire_date = Carbon::parse(gmdate('Y-m-d H:i:s',strtotime($userData->expire_date)));
        $new_expire_date = null;
        $d = 0;
        if(Carbon::now()>$expire_date){
            $new_expire_date = Carbon::now()->addDays($days);
            $d = 2;
        }else{
            $new_expire_date = Carbon::parse(gmdate('Y-m-d H:i:s',strtotime($userData->expire_date)))->addDays($days);
            $d = 3;
        }
        $userData->payed_date = Carbon::now()->format('Y-m-d H:i:s');
        $userData->expire_date = $new_expire_date->format('Y-m-d H:i:s');
        $userData->days_left = $new_expire_date->diffInDays(Carbon::now());
        $userData->save();
        
        return response()->json([
            'data' => $userData
        ]);
    }
     
    public function changeUserPassword(Request $request){
        $adminUserId = $request->user()->id;
        if($this->isUserAllowed($adminUserId)==false){
            return response()->json([
                'data' => 403
            ]);
        }
        
        $userId = $request->json('user_id');
        $newPassword = $request->json('new_password');   
        User::where('id',$userId)->update(['password'=>bcrypt($newPassword)]);
        return response()->json([
            'data' => 200
        ]);
    }

    public function setStoreBlocked(Request $request){
        $adminUserId = $request->user()->id;
        if($this->isUserAllowed($adminUserId)==false){
            return response()->json([
                'data' => 403
            ]);
        }
        
        $storeId = $request->json('store_id');
        $store = Store::where('id',$storeId)->update(['blocked'=>true,'block_text'=>$request->json('text')]);
        return response()->json([
            'data' => Store::where('id',$storeId)->first()
        ]);
    }

    public function setStoreUnBlocked(Request $request){
        $adminUserId = $request->user()->id;
        if($this->isUserAllowed($adminUserId)==false){
            return response()->json([
                'data' => 403
            ]);
        }
        
        $storeId = $request->json('store_id');
        $store = Store::where('id',$storeId)->update(['blocked'=>false]);
        return response()->json([
            'data' => Store::where('id',$storeId)->first()
        ]);
    }


    private function isUserAllowed($userId){
         $roleMng = new RoleManager();
         return $roleMng->isAdmin($userId)||$roleMng->isAdsAdmin($userId);
    }
}
