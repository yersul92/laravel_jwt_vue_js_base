<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;
use App\Role;
use App\User;
use App\Http\Middleware\RoleManager;
class AdsController extends Controller
{
    public function index(Request $request){
        $ads = Ad::where('publish',true)->orderBy('ad_order')->get();
        return response()->json([
            'data' => $ads
        ]);
    }
    
    public function save(Request $request){
        $userId = $request->user()->id;
        if($this->isUserAllowed($userId)==false){
            return response()->json([
                'data' => 403
            ]);     
        }
        $adId = $request->json('id'); 
        $ad = null;
        $adData =  (object)array (
            "id" => $request->json('id'),
            "name" => $request->json('name'),
            "phone" => $request->json('phone'),
            "head_kz" => $request->json('head_kz'),
            "head_ru" => $request->json('head_ru'),
            "text_kz" => $request->json('text_kz'),
            "text_ru" => $request->json('text_ru'),
            "img_url" => $request->json('img_url'),
            "publish" => $request->json('publish'),
            "expire_date" => $request->json('expire_date'),
            "tmp_img_url" => $request->json('tmp_img_url'),
            "url" => $request->json('url'),
        );
        if($adData->publish==true){
            $adData->publish_date=date('Y-m-d');
            if(!isset($adData->expire_date)){
                return response()->json([
                    'error' => 'expire_date'
                ],422);
             }
        }
        //else if($adData->publish==false){
        //      $adData->expire_date=date();
        //}
        if($adId==0){
            $adData->img_url = $adData->tmp_img_url;
            $ad = Ad::create((array)$adData); 
            copy(public_path().'/photos/tmp/original/'.$adData->img_url,public_path().'/photos/ads/'.$adData->img_url);
            $this->deleteFile(public_path().'/photos/tmp/original/'.$adData->img_url);
            $this->deleteFile(public_path().'/photos/tmp/thumbnail/'.$adData->img_url);
        }else{
            $ad = Ad::findOrFail($adId);    
            if(isset($adData->tmp_img_url)){
                copy(public_path().'/photos/tmp/original/'.$adData->tmp_img_url,public_path().'/photos/ads/'.$adData->tmp_img_url);
                $this->deleteFile(public_path().'/photos/tmp/original/'.$adData->tmp_img_url);
                $this->deleteFile(public_path().'/photos/tmp/thumbnail/'.$adData->tmp_img_url);
                $this->deleteFile(public_path().'/photos/ads/'.$ad->img_url);
                $adData->img_url=$adData->tmp_img_url;
            }
            $ad->update((array)$adData);           
        }
        return response()->json([
            'data' => $ad
        ]);
    }

    public function remove(Request $request){
        $userId = $request->user()->id;
        if($this->isUserAllowed($userId)==false){
        return response()->json([
            'data' => 403
        ]);     
        }
        $adId = $request->json('id'); 
        $ad = Ad::findOrFail($adId);    
        $ad->delete();
        $this->deleteFile(public_path().'/photos/ads/'.$ad->img_url);
        return response()->json([
            'data' => 200
        ]);
    }

    private function isUserAllowed($userId){
         $roleMng = new RoleManager();
         return $roleMng->isAdsAdmin($userId);
    }

    public function ads(Request $request){
        $userId = $request->user()->id;
        if($this->isUserAllowed($userId)==false){
            return response()->json([
                'data' => 403
            ]);
        }
        $ads = Ad::orderByDesc('id')->paginate(30);
        return response()->json([
            'data' => $ads
        ]);
    }

    public function adsByText(Request $request,$text){
        $userId = $request->user()->id;
        if($this->isUserAllowed($userId)==false){
            return response()->json([
                'data' => 403
            ]);
        }     
        $ads = Ad::where('name','LIKE','%' . $text . '%')
        ->orWhere('head_kz','LIKE','%' . $text . '%')
        ->orWhere('head_ru','LIKE','%' . $text . '%')->paginate(30);
        return response()->json([
            'data' => $ads
        ]);
    }
    
    public function deleteFile($filePath){
        if(file_exists($filePath)){
            unlink($filePath);
        }
    }
}
