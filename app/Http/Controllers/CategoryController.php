<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $arr = Category::all();;
        $new = array();
        foreach ($arr as $a){
            $new[$a['parent_id']][] = $a;
        }
        $tree = $this->createTree($new, $new[0]); // changed
        //print_r($tree);

        
        return response()->json([
            'data' => $tree
        ]);
    }

    function createTree(&$list, $parent){
            $tree = array();
            foreach ($parent as $k=>$l){
                if(isset($list[$l['id']])){
                    $l['children'] = $this->createTree($list, $list[$l['id']]);
                }
                $tree[] = $l;
            } 
            return $tree;
        }
}
