<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\StorePhone;
use App\StoreCategory;
use App\Product;
use App\ProductPhoto;
use App\User;
use App\Role;
use App\UserRole;
use App\Http\Requests\StoreFormRequest;
use App\Http\Requests\ProductFormRequest;
use App\Http\Middleware\RoleManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $roleMng = new RoleManager();
        $data = [];
        $roles = [];
        $data['user'] = $request->user();
        $userRoles = User::with('roles')->where('id',$request->user()->id)->first()->roles;
        $data['roles'] = $userRoles;
        return response()->json([
            'data' => $data,
        ]);
    }

    public function saveName(Request $request){
        $userId = $request->user()->id;
        $name = $request->json('name');   
        User::where('id',$userId)->update(['name'=>$name]);
        return response()->json([
            'data' => $name
        ]);
    }
    // TODO: keyin sms podtverzhdenye jasatu kerek
    public function changePassword(Request $request){
        $userId = $request->user()->id;
        $newPassword = $request->json('new_password');   
        User::where('id',$userId)->update(['password'=>bcrypt($newPassword)]);
        return response()->json([
            'data' => 200
        ]);
    }

    public function setAgreed(Request $request){
        $userId = $request->user()->id;
        $agreed = $request->json('agreed');   
        User::where('id',$userId)->update(['agreed'=>$agreed,'agreed_date'=>Carbon::now()->format('Y-m-d H:i:s')]);
        return response()->json([
            'data' => $agreed
        ]);
    }

    public function setPayed(Request $request){
        $userId = $request->user()->id;
        $cost = $request->json('cost');
        $days = $request->json('days');
        $degree = 3;
        if($cost==2000||$cost==9990||$cost==16990){
            $degree = 3;
        }else if($cost==2500||$cost==12990||$cost==22990){
            $degree = 2;
        }else if($cost==3000||$cost==15990||$cost==28990){
            $degree = 1;
        }
        //TODO : payment history
        $userData= User::where('id',$userId)->get()->first();
        Store::where('user_id',$userId)->update(['degree'=>$degree]);
        Product::where('user_id',$userId)->update(['degree'=>$degree]);

        $expire_date = Carbon::parse(gmdate('Y-m-d H:i:s',strtotime($userData->expire_date)));
        $new_expire_date = null;
        $d = 0;
        if(Carbon::now()>$expire_date){
            $new_expire_date = Carbon::now()->addDays($days);
            $d = 2;
        }else{
            $new_expire_date = Carbon::parse(gmdate('Y-m-d H:i:s',strtotime($userData->expire_date)))->addDays($days);
            $d = 3;
        }
        $userData->payed_date = Carbon::now()->format('Y-m-d H:i:s');
        $userData->expire_date = $new_expire_date->format('Y-m-d H:i:s');
        $userData->days_left = $new_expire_date->diffInDays(Carbon::now());
        $userData->save();
        return response()->json([
            'data' => 200
        ]);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $store = $this->getStoreByUserId($user->id);
        return response()->json([
            'data' => $store,
        ]);
    }

    public function product(Request $request,$id)
    {
        $user = $request->user();
        $product = Product::with('photos')->where('user_id',$user->id)->where('id',$id)->first();
        return response()->json([
            'data' => $product
        ]);
    }
     
    private function getStoreByUserId($userId){
         return Store::with(array('products','products.photos'))->where('user_id',$userId)->first(); 
    } 
    
    public function saveStore(StoreFormRequest $request){
        $userId = $request->user()->id;
        if($request->user()->days_left<1){
            return response()->json([
                'error' => 'user not allowed to edit data'
            ],500);    
        }
        
        $id = $request->json('id'); 
        $name = $request->json('name');   
        $address = $request->json('address');   
        $comment = $request->json('comment');   
        $photo_path = $request->json('photo_path');   
        $phone = $request->json('phone');   
        $tmp_photo = $request->json('tmp_photo');
        
        $isset = false;
        $store = null;
        if($id==0){
            copy(public_path().'/photos/tmp/original/'.$tmp_photo,public_path().'/photos/stores/original/'.$tmp_photo);
            copy(public_path().'/photos/tmp/thumbnail/'.$tmp_photo,public_path().'/photos/stores/thumbnail/'.$tmp_photo);
            $user = User::where('id', $userId)->first();
            $user->has_store = true;
            $user->days_left = 3;
            $user->payed_date = Carbon::now()->format('Y-m-d H:i:s');
            $user->expire_date = Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
            $user->save();

            $store = Store::create([
                'name'=>$name,
                'address'=>$address,
                'comment'=>$comment,
                'photo_path'=>$tmp_photo,
                'user_id'=>$userId,
                'city_id'=>-1,
                'degree'=>2,
                'phone'=>$phone,
                'store_order'=>$userId
            ]);
            
            $this->deleteFile(public_path().'/photos/tmp/original/'.$tmp_photo);
            $this->deleteFile(public_path().'/photos/tmp/thumbnail/'.$tmp_photo);
            
        }else{
            $store = Store::where('id',$id)->first();
            $store->name = $name;
            $store->address = $address;
            $store->comment = $comment;
            $store->phone = $phone;
            if($tmp_photo){
                copy(public_path().'/photos/tmp/original/'.$tmp_photo,public_path().'/photos/stores/original/'.$tmp_photo);
                copy(public_path().'/photos/tmp/thumbnail/'.$tmp_photo,public_path().'/photos/stores/thumbnail/'.$tmp_photo);
                $this->deleteFile(public_path().'/photos/stores/original/'.$store->photo_path);
                $this->deleteFile(public_path().'/photos/stores/thumbnail/'.$store->photo_path);
                $this->deleteFile(public_path().'/photos/tmp/original/'.$tmp_photo);
                $this->deleteFile(public_path().'/photos/tmp/thumbnail/'.$tmp_photo);
                $store->photo_path = $tmp_photo;
            }
            $store->save();
        }
        
        $store = null;
        $store = Store::with(array('products','products.photos','user'))->where('id',$id)->first();
        return response()->json([
            'data' => $store,
            'isset'=>$isset
        ]);
    }

    public function saveProduct(ProductFormRequest $request){
       
        $userId = $request->user()->id;
        if($request->user()->days_left<1){
            return response()->json([
                'error' => 'user not allowed to edit data'
            ],500);    
        }

        $store = Store::where('user_id',$userId)->first();
        $photosFrom = $request->json('photos');
        $productId = $request->json('id'); 
        $productData =  (object)array (
            "id" => $request->json('id'),
            "name" => $request->json('name'),
            'comment'=>$request->json('comment'),
            'cost'=>$request->json('cost'),
            'category_id'=>$request->json('category_id'),
        );
        $productData->user_id = $userId;
        $productData->store_id = $store->id;
        $productData->degree = $store->degree;
        $product = null;
        if($productId==0){
            $product = Product::create((array)$productData); 
            $photos = [];
            foreach ($photosFrom as $photo) {
                $photos[] = new ProductPhoto($photo);
                copy(public_path().'/photos/tmp/original/'.$photo['path'],public_path().'/photos/products/original/'.$photo['path']);
                copy(public_path().'/photos/tmp/thumbnail/'.$photo['path'],public_path().'/photos/products/thumbnail/'.$photo['path']);
                $this->deleteFile(public_path().'/photos/tmp/original/'.$photo['path']);
                $this->deleteFile(public_path().'/photos/tmp/thumbnail/'.$photo['path']);
            }
            $product->photos()->saveMany($photos);
        }else{
            $product = Product::with('photos')->findOrFail($productId);    
            
            $photoExistingIds = [];
            foreach ($photosFrom as $photo) {
                
                if(isset($photo['id'])){
                    array_push($photoExistingIds, $photo['id']);                        
                    ProductPhoto::whereId($photo['id'])->update(['current'=>$photo['current']]);
                }else{
                    $newPhoto = ProductPhoto::create([
                        'product_id'=>$productId,
                        'path'=>$photo['path'],
                        'current'=>$photo['current']
                    ]);
                    array_push($photoExistingIds, $newPhoto->id);                        
                    copy(public_path().'/photos/tmp/original/'.$photo['path'],public_path().'/photos/products/original/'.$photo['path']);
                    copy(public_path().'/photos/tmp/thumbnail/'.$photo['path'],public_path().'/photos/products/thumbnail/'.$photo['path']);
                    $this->deleteFile(public_path().'/photos/tmp/original/'.$photo['path']);                  
                    $this->deleteFile(public_path().'/photos/tmp/thumbnail/'.$photo['path']);                  
                }
            }
            if(count($photoExistingIds)>0){
                ProductPhoto::whereProductId($product->id)->whereNotIn('id',$photoExistingIds)->delete();
            }else{
                foreach ($product->photos as $productPhoto) {
                    if(array_search($productPhoto->id,$photoExistingIds)<0){
                       $this->deleteFile(public_path().'/photos/products/original/'.$productPhoto->path); 
                       $this->deleteFile(public_path().'/photos/products/thumbnail/'.$productPhoto->path); 
                    }                    
                }
                ProductPhoto::whereProductId($product->id)->delete();
            }
            $product->update((array)$productData);
        }
        $this->updateStoreCategories($store->id);
        $this->updateStoreCurrentProduct($store->id);
        
        return response()->json([
            'data' => Product::with('photos')->where('id',$product->id)->first()
        ]);
    }
    
    function updateStoreCategories($storeId){
        StoreCategory::whereStoreId($storeId)->delete();         
        $products = Product::whereStoreId($storeId)->get();
        if(isset($products)){
            $cat_ids = array();
            $parent_cat_ids = array();
            foreach($products as $product){
                if(in_array($product->category_id,$cat_ids)){

                }else{
                    array_push($cat_ids,$product->category_id);
                }
            }            
            
            foreach($cat_ids as $cat_id){
                $allParents = DB::select('SELECT T2.id,T2.parent_id FROM ( SELECT @r AS _id, (SELECT @r := parent_id FROM categories WHERE id = _id) AS parent_id, @l := @l + 1 AS lvl FROM (SELECT @r := '.$cat_id.', @l := 0) vars, categories m WHERE @r <> 0) T1 JOIN categories T2 ON T1._id = T2.id ORDER BY T1.lvl DESC');
                if(isset($allParents)){
                    foreach($allParents as $parentCatId){
                        if(in_array($parentCatId->id,$parent_cat_ids)){

                        }else{
                            array_push($parent_cat_ids,$parentCatId->id);
                        }
                    }
                }
                StoreCategory::create([
                    'store_id'=>$storeId,
                    'category_id'=>$cat_id
                ]);    
            }
            foreach($parent_cat_ids as $parent_cat_id){
                 StoreCategory::create([
                    'store_id'=>$storeId,
                    'category_id'=>$parent_cat_id
                ]);    
            }
        }
    }

    function updateStoreCurrentProduct($storeId){
        $products = Product::whereStoreId($storeId)->get();
        if(isset($products)){
            $i = 0;
            foreach($products as $product){
                if($i==0){
                    Product::where('id',$product->id)->update(['current'=>true,'product_order'=>$product->id]);
                }
                else{
                    Product::where('id',$product->id)->update(['current'=>false]);
                }
                $i=$i+1;
            }
        }
    }

    public function deleteProduct(Request $request,$id){
        $userId = $request->user()->id;
        $product = Product::with(['photos','store'])->findOrFail($id);
        if($userId!=$product->user_id){
            return response()->json([
            401
        ],401);    
        }
        if(isset($product->photos)){
            foreach($product->photos as $photo){
                $this->deleteFile(public_path().'/photos/products/original/'.$photo->path); 
                $this->deleteFile(public_path().'/photos/products/thumbnail/'.$photo->path); 
            }
        }
        ProductPhoto::whereProductId($product->id)->delete();
        Product::whereId($product->id)->delete();
        $this->updateStoreCategories($product->store->id);
        $this->updateStoreCurrentProduct($product->store->id);
        return response()->json([
            200
        ]);
    }    
        
    function deleteFile($filePath){
        if(file_exists($filePath)){
            unlink($filePath);
        }
    }

}
