<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use Image;
class ImageController extends Controller
{
    public function upload(Request $request){

        $postData = $request->only('file');
        $isAds = $request->only('ads');
        $file = $postData['file'];

        // Build the input for validation
        $fileArray = array('image' => $file);

        // Tell the validator that this file should be an image
        $rules = array(
        'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        );

        // Now pass the input and rules into the validator
        $validator = Validator::make($fileArray, $rules);

        // Check to see if validation fails or passes
        if ($validator->fails())
        {
            // Redirect or return json to frontend with a helpful message to inform the user 
            // that the provided file was not an adequate type
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        } else
        {
            // Store the File Now
            // read image from temporary file
            $extension =  $file->clientExtension();
            $newFileName = uniqid('img_' . date('YmdHms') . '-'.rand(100000, 999999)).'.'.$extension;
            if(isset($isAds)){
                Image::make($file)->resize(675, 355, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path().'/photos/tmp/original/'.$newFileName); 
            }else{
                Image::make($file)->resize(600, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path().'/photos/tmp/original/'.$newFileName);
            }
            Image::make($file)->resize(170, 132, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path().'/photos/tmp/thumbnail/'.$newFileName);
            
            return response()->json(['file_name'=>$newFileName], 200);
        }        
    }
    public function delete(Request $request){
        $fileName = $request->json('file_name');
        if(file_exists(public_path().'/photos/tmp/original/'.$fileName)){
            unlink(public_path().'/photos/tmp/original/'.$fileName);
        }
        if(file_exists(public_path().'/photos/tmp/thumbnail/'.$fileName)){
            unlink(public_path().'/photos/tmp/thumbnail/'.$fileName);
        }
        return response()->json(['file_name'=>$fileName], 200);
    }
    
    public function uploadOldImages(Request $request){
              
    }
}
