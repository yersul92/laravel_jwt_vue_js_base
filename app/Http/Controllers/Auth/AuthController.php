<?php

namespace App\Http\Controllers\Auth;

use App\NotConfirmedUser;
use App\User;
use App\PhoneConfirm;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterFormRequest;
use App\Http\Requests\PhoneConfirmFormRequest;
use App\Http\Requests\ConfirmRegisterFormRequest;
use Carbon\Carbon;
use JWTAuth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Services\KcellApi\KcellApiClient;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Message;


class AuthController extends Controller
{
    public function __construct() {

    }
    
    public function user_(Request $request,$id){

        //$userData = User::with('roles')->where('id',$id)->first();
        return response()->json([
                'data' => 200
            ], 200);
    }

    public function confirmRegisterByCode(ConfirmRegisterFormRequest $request){
        try {
            $userId = $request->json('user_id'); 
            $code = $request->json('code'); 
            $notConfirmedUser = NotConfirmedUser::where('confirm_code', $code)->where('id', $userId)->first();               
            if(isset($notConfirmedUser)&&isset($notConfirmedUser->id)){
               $notConfirmedUser->registered = true;
               $notConfirmedUser->save();
               User::create([
                   'name'=>$notConfirmedUser->name,
                   'phone'=>$notConfirmedUser->phone,
                   'password'=>$notConfirmedUser->password
               ]);
               return response()->json(['text'=>'success'], 200);
            }else{
               return response()->json(['text'=>'error'], 401);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => $e
            ], 500);
        }      

    }
    
    private function randomPassword() {
        $alphabet = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function resetPassword(Request $request){
        try {
            $phone = $request->json('phone');
            $newPassword = $this->randomPassword(); 
            $user = User::where('phone', $phone)->first();
            if(isset($user)){
                  $user->password=bcrypt($newPassword);
                  $sendStatus = $this->sendNewPassword($phone,$newPassword);        
                  //TODO if($sendStatus!=201)
                  $user->save();
                  return response()->json($sendStatus);
            }else{
                return response()->json([
                    'error'=>"user not found"
                ], 401);   
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => $e
            ], 500);
        }      

    }

    public function register(RegisterFormRequest $request)
    {
        try {
            $phone = $request->json('phone');
            $registeredUser = User::where('phone',$phone)->first();         
            if(isset($registeredUser)&&isset($registeredUser->id)){
                return response()->json([ 'error'=>'user_exists'], 418);
            }

            $randNumber = rand(10000, 99999); 
            $sendStatus = $this->sendCode($phone,$randNumber);        
            if($sendStatus!=201){
                return response()->json([ 'error'=>'send_message_error'], 500);
            }
            $notConfirmedUser = NotConfirmedUser::where('phone', $request->json('phone'))->first();
            $user_id = null;
            if(isset($notConfirmedUser)&&isset($notConfirmedUser->phone)){
                $notConfirmedUser->name = $request->json('name');
                $notConfirmedUser->password = bcrypt($request->json('password'));
                $notConfirmedUser->confirm_code=$randNumber;
                $notConfirmedUser->save();                
                $user_id = $notConfirmedUser->id;
            }else{
                $newUser = NotConfirmedUser::create([
                    'name' => $request->json('name'),
                    'phone' => $request->json('phone'),
                    'password' => bcrypt($request->json('password')),
                    'confirm_code'=>$randNumber
                ]);
                $user_id = $newUser->id;
            }
            return response()->json(['user_id'=>$user_id], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => $e
            ], 500);
        }
    }

    public function signin(Request $request)
    {
        try {
            $token = JWTAuth::attempt($request->only('phone', 'password'), [
                'exp' => Carbon::now()->addWeek()->timestamp,
            ]);
        } catch (JWTException $e) {
            return response()->json([
                'error' => 'Could not authenticate',
            ], 500);
        }

        if (!$token) {
            return response()->json([
                'error' => 'Could not authenticate',
            ], 401);
        } else {
            $data = [];
            $meta = [];

            $data['phone'] = $request->user()->phone;
            $data['name'] = $request->user()->name;
            $meta['token'] = $token;
            $userRoles = User::with('roles')->where('id',$request->user()->id)->first()->roles;
            $data['roles'] = $userRoles; 
            return response()->json([
                'data' => $data,
                'meta' => $meta
            ]);
        }
    }

    private function sendCode($phone,$code)
    {
        $messageText = "BarBazar code: ".$code;
        $message = Message::create([
            'phone'=>$phone,
            'text'=> $messageText
        ]);
        
        $messageId = env('KcellApiAI', '23623555')+$message->id;
        $sendData = array(
            "client_message_id"=>$messageId,
            "sender"=>"Bar Bazar",
            "recipient"=>$phone,
            "time_bounds"=>"full",
            "message_text"=>$messageText
        );
        $client = new Client();
        $res = $client->request('POST', 'https://api.kcell.kz/app/smsgw/rest/v2/messages', 
        [
            'json' => $sendData, 
            'auth' => ['barbazar', 'Bar%^&Bazar'],
            'verify'=> false
        ]);
        $message->send_status=$res->getStatusCode();
        $message->save();

        return $res->getStatusCode();
    }

    private function sendNewPassword($phone,$text)
    {
        $messageText = 'BarBazar.kz пароль: '.$text;
        $message = Message::create([
            'phone'=>$phone,
            'text'=> $messageText
        ]);
        $messageId = env('KcellApiAI', '23623555')+$message->id;
        $sendData = array(
            "client_message_id"=>$messageId,
            "sender"=>"Bar Bazar",
            "recipient"=>$phone,
            "time_bounds"=>"full",
            "message_text"=>$messageText
        );
        $client = new Client();
        $res = $client->request('POST', 'https://api.kcell.kz/app/smsgw/rest/v2/messages', 
        [
            'json' => $sendData, 
            'auth' => ['barbazar', 'Bar%^&Bazar'],
            'verify'=> false
        ]);
        $message->send_status=$res->getStatusCode();
        $message->save();
        return $res->getStatusCode();
    } 

}