<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductMix;

class ProductController extends Controller
{
    public function index(Request $request,$degree)
    {
        $paginaton = 48;
        if($degree==2){
           $paginaton = 24;     
        }else if($degree==3){
           $paginaton = 24;     
        }
        $products = Product::with('photos','user','store')->whereHas('user' , function ($query) {
            $query->where('days_left','>', 0);})->whereHas('store' , function ($query) {
            $query->where('blocked',false);})->where('current',true)->where('degree',$degree)->orderBy('product_order')->paginate($paginaton);
        return response()->json([
            'data' => $products
        ]);
    }

    public function index2(Request $request)
    {
        $paginaton = 48;
        $products = Product::with('photos','user','store')->whereHas('user' , function ($query) {
            $query->where('days_left','>', 0);})->whereHas('store' , function ($query) {
            $query->where('blocked',false);})->orderBy('product_order')->paginate($paginaton);
        return response()->json([
            'data' => $products
        ]);
    }
    
    public function getById(Request $request,$id)
    {
        $product = Product::with(array('photos','user','store'))->where('id',$id)->first();
        return response()->json([
            'data' => $product
        ]);
    }
}
