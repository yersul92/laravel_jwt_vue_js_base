<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StoreCategory;
use App\Store;
use App\Product;
class StoreController extends Controller
{
    public function index(Request $request,$degree,$category)
    {
        $paginaton = 21;
        if($degree==2){
           $paginaton = 20;     
        }else if($degree==3){
           $paginaton = 24;     
        }
        //TODO Уақыты өтіп кеткендерді шығармау
        //TODO blocked -ti eskeru
        //orderby order_id
        $storeByCats = Store::whereHas('categories' , function ($query) use ($category) {
            $query->where('category_id', $category);})->whereHas('user' , function ($query) {
            $query->where('days_left','>', 0);})->where('blocked',false)->where('degree',$degree)->orderBy('degree')->paginate($paginaton);
        return response()->json([
            'data' => $storeByCats
        ]);
    }

    public function storesByCat(Request $request,$category)
    {
        $paginaton = 48;
        $storeByCats = Store::whereHas('categories' , function ($query) use ($category) {
            $query->where('category_id', $category);})->whereHas('user' , function ($query) {
            $query->where('days_left','>', 0);})->where('blocked',false)->orderBy('degree')->paginate($paginaton);
        return response()->json([
            'data' => $storeByCats
        ]);
    }
    /*
    public function storesByCatChangeOrder(Request $request,$category)
    {
        $paginaton = 48;
        $storeByCats = Store::whereHas('categories' , function ($query) use ($category) {
            $query->where('category_id', $category);})->whereHas('user' , function ($query) {
            $query->where('days_left','>', 0);})->where('blocked',false)->orderBy('degree')->paginate($paginaton);
        return response()->json([
            'is_array' => is_array($storeByCats),
            'is_array_a' => is_object($storeByCats->data),
            'is_object' => is_object($storeByCats)
        ]);
    }
    */
    public function getById(Request $request,$id)
    {
        $store = Store::with(array('products','products.photos','user'))->where('id',$id)->first();
        return response()->json([
            'data' => $store
        ]);
    }

}
