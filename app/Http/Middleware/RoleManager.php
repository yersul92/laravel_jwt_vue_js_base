<?php

namespace App\Http\Middleware;

use App\User;
use App\Role;
class RoleManager
{
    private function getUserRoles($id){
        return User::with('roles')->where('id',$id)->first()->roles;
    }
    
    public function isAdmin($userId)
    {
        $userRoles = $this->getUserRoles($userId);
        if(!isset($userRoles)||count($userRoles)==0){
            return false;
        }
        foreach ($userRoles as $userRole) {
            if($userRole->name=='admin'){
                return true;
            }
        }
        return false;
    }

    public function isOperator($userId)
    {
        $userRoles = $this->getUserRoles($userId);
        if(!isset($userRoles)||count($userRoles)==0){
            return false;
        }
        foreach ($userRoles as $userRole) {
            if($userRole->name=='admin'||$userRole->name=='operator'){
                return true;
            }
        }
        return false;
    }

     public function isAdsAdmin($userId)
     {
        $userRoles = $this->getUserRoles($userId);
        if(!isset($userRoles)||count($userRoles)==0){
            return false;
        }
        foreach ($userRoles as $userRole) {
            if($userRole->name=='admin'||$userRole->name=='ads_admin'){
                return true;
            }
        }
        return false;
     }
}
