<?php

namespace App\Http\Middleware;

use Closure;

use App\User;
use App\Role;
class adsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = $request->user()->id;
        $user = User::with('roles')->where('id',$userId)->first();
        //if(!isset(user))
        //if(Role::isAdmin)
        return $next($request);
    }
}
