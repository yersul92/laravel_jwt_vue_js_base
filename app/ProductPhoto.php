<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'path','product_id','current',
    ];
}
