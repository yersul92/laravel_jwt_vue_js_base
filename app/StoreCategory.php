<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Store;

class StoreCategory extends Model
{
    public $timestamps = false;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id','category_id'
    ];
}
