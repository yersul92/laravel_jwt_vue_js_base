<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneConfirm extends Model
{
    protected $fillable = ['phone', 'code','confirmed'];
}
