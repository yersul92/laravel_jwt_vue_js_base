<?php namespace App\Services\KcellApi;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Message;

class KcellApiClient
{
    public function sendCode($phone,$code)
    {
        $messageText = "BarBazar code: ".$code;
        $message = Message::create([
            'phone'=>$phone,
            'text'=> $messageText
        ]);
        $messageId = 23623505+$message->id;
        $sendData = array(
            "client_message_id"=>$messageId,
            "sender"=>"Bar Bazar",
            "recipient"=>$phone,
            "time_bounds"=>"ad99",
            "message_text"=>$messageText
        );
        $client = new Client();
        $res = $client->request('POST', 'https://api.kcell.kz/app/smsgw/rest/v2/messages', 
        [
            'json' => $sendData, 
            'auth' => ['barbazar', 'Bar%^&Bazar'],
            'verify'=> false
        ]);
        $message->send_status=$res->getStatusCode();
        $message->save();

        return $res->getStatusCode();
    }

    public function sendNewPassword($phone,$text)
    {
        $messageText = 'BarBazar.kz пароль: '.$code;
        $message = Message::create([
            'phone'=>$phone,
            'text'=> $messageText
        ]);
        $messageId = 23623505+$message->id;
        $sendData = array(
            "client_message_id"=>$messageId,
            "sender"=>"Bar Bazar",
            "recipient"=>$phone,
            "time_bounds"=>"ad99",
            "message_text"=>$messageText
        );
        $client = new Client();
        $res = $client->request('POST', 'https://api.kcell.kz/app/smsgw/rest/v2/messages', 
        [
            'json' => $sendData, 
            'auth' => ['barbazar', 'Bar%^&Bazar'],
            'verify'=> false
        ]);
        $message->send_status=$res->getStatusCode();
        $message->save();

        return $res->getStatusCode();
    }
}