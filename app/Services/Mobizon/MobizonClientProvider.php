<?php 
namespace App\Services\Mobizon;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
class MobizonClientProvider extends LaravelServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Services\Mobizon\MobizonClientInterface',
            'App\Services\Mobizon\MobizonClient'
        );
    }
}