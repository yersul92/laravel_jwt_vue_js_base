<?php namespace App\Services\Mobizon;
interface MobizonClientInterface
{
    /**
     * @param string   $reciepent
     * @param data     $data to send
     * @return true if sent, false if not
     */
    public function send($reciepent, $data);
}