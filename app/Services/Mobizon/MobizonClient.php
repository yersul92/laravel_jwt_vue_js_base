<?php namespace App\Services\Mobizon;

require_once base_path('vendor/mobizon/mobizon-php/src/MobizonApi.php');
class MobizonClient implements MobizonClientInterface
{
    
    public function send($reciepent, $text){ 
        $api = new \Mobizon\MobizonApi('e549137c473771681b81fd0c9b1a395ca8ea30a5');
        //echo 'Send message...' . PHP_EOL;
        $alphaname = 'TEST';
        if ($api->call('message',
            'sendSMSMessage',
            array(
                'recipient' => $reciepent,
                'text'      => $text
            ))
        ) {
            $messageId = $api->getData('messageId');
            //echo 'Message created with ID:' . $messageId . PHP_EOL;
            if ($messageId) {
                return true;
                /*
                echo 'Get message info...' . PHP_EOL;
                $messageStatuses = $api->call(
                    'message',
                    'getSMSStatus',
                    array(
                        'ids' => array($messageId, '13394', '11345', '4393')
                    ),
                    array(),
                    true
                );
                if ($api->hasData()) {
                    foreach ($api->getData() as $messageInfo) {
                        echo 'Message # ' . $messageInfo->id . " status:\t" . $messageInfo->status . PHP_EOL;
                    }
                }
                */ 
            }
        } else {
            //echo 'An error occurred while sending message: [' . $api->getCode() . '] ' . $api->getMessage() . 'See details below:' . PHP_EOL;
            //var_dump(array($api->getCode(), $api->getData(), $api->getMessage()));
            return false;
        }
    }
}