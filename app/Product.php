<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductPhoto;
use App\Store;
use App\User;

class Product extends Model
{
    protected $fillable = [
        'name','comment','cost','category_id','store_id','user_id','degree','current'
    ];
    public function photos(){
        return $this->hasMany(ProductPhoto::class);
    }

    public function user(){
        return $this->belongsTo(User::class)->select(array('id','name', 'phone'));;
    }    

    public function store(){
        return $this->belongsTo(Store::class)->select(array('id','name', 'address'));;
    }    
}
