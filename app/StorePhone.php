<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Store;
class StorePhone extends Model
{
    public $timestamps = false;
    protected $fillable = ['store_id','phone'];
    public function store(){
        return $this->belongsTo(Store::class);
    }
}
