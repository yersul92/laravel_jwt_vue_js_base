<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotConfirmedUser extends Model
{
    protected $fillable = ['name','phone', 'password', 'confirm_code'];
}
