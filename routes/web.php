<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('api/csrf', function() {
    return Session::token();
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test_mobile', function () {
    return view('test_mobile');
});

/*
Route::get('/phpinfo', function () {
    return view('phpinfo');
});

Route::get('/user_/{id}',[
        'uses' => 'Auth\AuthController@user_',
    ]);

Route::get('/mongo/{text}',[
        'uses' => 'SearchController@get',
    ]);

Route::get('/mongoinsert',[
        'uses' => 'SearchController@insert',
    ]);

*/

Route::get('/test', function () {
    return view('coming_soon');
});


Route::group(['middleware' =>'api','prefix'=>"/api"], function () {
    
    Route::get('search/query_text/{query_text}', [
        'uses' => 'SearchController@get',
    ]);

    Route::get('ads', [
        'uses' => 'AdsController@index',
    ]);

    Route::get('categories', [
        'uses' => 'CategoryController@index',
    ]);
    
    Route::get('stores/degree/{degree}/category/{category}',[
        'uses' => 'StoreController@index'
    ]);

    Route::get('stores/category/{category}',[
        'uses' => 'StoreController@storesByCat'
    ]);

    Route::get('stores/{id}',[
        'uses' => 'StoreController@getById'
    ]);

    Route::get('products/degree/{degree}',[
        'uses' => 'ProductController@index'
    ]);

    Route::get('products',[
        'uses' => 'ProductController@index2'
    ]);
    
    Route::get('products/{id}',[
        'uses' => 'ProductController@getById'
    ]);

    /*
    

    Route::get('sendtest', [
        'uses' => 'Auth\AuthController@testSend',
    ]);
    */
    
    Route::post('register', [
        'uses' => 'Auth\AuthController@register',
    ]);

    Route::post('confirm_register', [
        'uses' => 'Auth\AuthController@confirmRegisterByCode',
    ]);

    Route::post('signin', [
        'uses' => 'Auth\AuthController@signin',
    ]);

    Route::post('sendnewpassword', [
        'uses' => 'Auth\AuthController@resetPassword',
    ]);

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('user', [
            'uses' => 'UserController@index',
        ]);
        
        Route::post('user/name', [
            'uses' => 'UserController@saveName',
        ]);
        
        Route::post('user/agreed', [
            'uses' => 'UserController@setAgreed',
        ]);

        Route::post('user/payed', [
            'uses' => 'UserController@setPayed',
        ]);

        Route::post('user/changepassword', [
            'uses' => 'UserController@changePassword',
        ]);
 
        Route::get('userstore', [
            'uses' => 'UserController@store',
        ]);

        Route::get('userproduct/{id}', [
            'uses' => 'UserController@product',
        ]);

        Route::post('userstore', [
            'uses' => 'UserController@saveStore',
        ]);

        Route::post('userproduct', [
            'uses' => 'UserController@saveProduct',
        ]); 

        Route::post('userproduct/delete/{id}', [
            'uses' => 'UserController@deleteProduct',
        ]); 
       
        Route::post('delete_tmp', [
            'uses' => 'ImageController@delete',
        ]);

        Route::post('upload_tmp', [
            'uses' => 'ImageController@upload',
        ]);
        
        Route::get('admin/users',[
            'uses' => 'AdminController@users'
        ]);
        Route::get('admin/users/search/{text}',[
            'uses' => 'AdminController@userByText'
        ]);

        Route::post('admin/users/setuserpayed',[
            'uses' => 'AdminController@setUserPayed'
        ]);

        Route::post('admin/users/changepassword',[
            'uses' => 'AdminController@changeUserPassword'
        ]);

        Route::post('admin/users/blockstore',[
            'uses' => 'AdminController@setStoreBlocked'
        ]);

        Route::post('admin/users/unblockstore',[
            'uses' => 'AdminController@setStoreUnBlocked'
        ]);

        Route::post('admin/ads', [
            'uses' => 'AdsController@save',
        ]);

        Route::get('admin/ads',[
            'uses' => 'AdsController@ads'
        ]);
        Route::get('admin/ads/search/{text}',[
            'uses' => 'AdsController@adsByText'
        ]);

    }); 
});