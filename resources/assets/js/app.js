require('./bootstrap');
window.Vue = require('vue');
import App from './components/App.vue';
import router from './router.js';
Vue.use(require('vue-resource'));

Vue.http.headers.common['X-CSRF-TOKEN'] = document.getElementsByName('csrf-token')[0].getAttribute('content');
Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');
Vue.http.options.root = '/';

//recursive used components

Vue.component('category-treenode', require('./components/Category/TreeNode.vue'));
Vue.component('category-menunode', require('./components/Category/MenuNode.vue'));
Vue.component('category-menutreenode', require('./components/Category/MenuTreeNode.vue'));
const app = new Vue({
    el: '#app',
    router: router,
    render: app => app(App)
});


