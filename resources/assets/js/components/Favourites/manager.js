export default{
    stores:[],
    products:[],
    hasLoadedFromLocalhost:false,
    AddProduct(product){
       let productExist = false;
       for (var i = 0; i < this.products.length; i++) {
           if(this.products[i].id==product.id){
               productExist=true;
           }           
       }
       if(productExist==false){          
           this.products.push({
               id:product.id,
               name:product.name,
               comment:product.comment,
               cost:product.cost,
               category_id:product.category_id,
               store_id:product.store_id,
               user_id:product.user_id,
               degree:product.degree,
               product_order:product.product_order,
               photos:product.photos,
               store:{
                   id:product.store.id,
                   name:product.store.name,
                   phone:product.store.phone,
                   address:product.store.address
               },
               user:{
                   name:product.user.name,
                   phone:product.user.phone
               }
           });
       }
       this.saveAtLocalhost();
    },
    RemoveProduct(product){
       for (var i = 0; i < this.products.length; i++) {
           if(this.products[i].id==product.id){
               this.products.splice(i,1);
           }           
       }
       this.saveAtLocalhost();
    },
    RemoveStore(store){
        for (var i = 0; i < this.stores.length; i++) {
           if(this.stores[i].id==store.id){
               this.stores.splice(i,1);
           }           
        }
        this.saveAtLocalhost();
    },
    AddStore(store){
       let storeExist = false;
       for (var i = 0; i < this.stores.length; i++) {
           if(this.stores[i].id==store.id){
               storeExist=true;
           }           
       }
       if(storeExist==false){
           store.products = null;
           
           this.stores.push({
               id:store.id,
               photo_path:store.photo_path,
               name:store.name,
               degree:store.degree
           });
       }
       this.saveAtLocalhost();
    },
    loadFromLocalhost(){
        this.products = JSON.parse(localStorage.getItem('barbazar_favourite_products'));
        if(!this.products){
            this.products = [];
        }
        this.stores = JSON.parse(localStorage.getItem('barbazar_favourite_stores'));
        if(!this.stores){
            this.stores = [];
        }
    },
    saveAtLocalhost(){
        localStorage.setItem('barbazar_favourite_products',JSON.stringify(this.products));
        localStorage.setItem('barbazar_favourite_stores',JSON.stringify(this.stores));
    }
}