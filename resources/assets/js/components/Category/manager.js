import Vue from 'vue';
import translate from '../../translate.js';

export default {
    categories:[],
	categoriesNotTree:[],
    fetchCategories(){
			Vue.http.get(
					'api/categories',
				).then(response => {
					//this.categories = response.data.data;
					for(let i=0;i<response.data.data.length;i++){
						let word = {
							value:'',
							kz:response.data.data[i].name_kz,
							ru:response.data.data[i].name_ru
						};
						let fullText = {
							value:'',
							kz:response.data.data[i].name_kz,
							ru:response.data.data[i].name_ru
						}

						translate.setNewWord(word);
						translate.setNewWord(fullText);
						let node = {
							id:response.data.data[i].id,
							order:response.data.data[i].order,
							icon:response.data.data[i].icon,
							word:word,
							fullText:fullText
						};
						if(response.data.data[i].children){
							node.children = this.loopChildren(response.data.data[i].children,fullText);
						}
						this.categories.push(node);
						this.categoriesNotTree[response.data.data[i].id] = node;
					}
					this.categories.sort(function(a,b) {
						return a.order - b.order;
					});

					console.log("cats got");
				},response => {
					console.log("cats got error");
				});	
			},
			loopChildren(children,parentFullText){
               let nodes = [];
			   for(let i=0;i<children.length;i++){
					let word = {
						value:'',
						kz:children[i].name_kz,
						ru:children[i].name_ru
					};
					let fullText = {
						value:'',
						kz:parentFullText.kz+' / '+children[i].name_kz,
						ru:parentFullText.ru+' / '+children[i].name_ru
					}
					translate.setNewWord(word);
					translate.setNewWord(fullText);
					let node = {
						id:children[i].id,
						order:children[i].order,
						icon:children[i].icon,
						word:word,
						fullText:fullText
					};
                    if(children[i].children){
					    node.children = this.loopChildren(children[i].children,fullText);
					}
					nodes.push(node); 
					this.categoriesNotTree[children[i].id] = node;
				}
				return nodes;
			},
			searchTreeByName(searchString){
				let resultNodes = [];
				for(let i=0; i < this.categories.length; i++){
					if(this.categories[i].word.ru.toLowerCase().indexOf(searchString.toLowerCase())>-1||this.categories[i].word.kz.toLowerCase().indexOf(searchString.toLowerCase())>-1){
						
						resultNodes.push({
							id:this.categories[i].id,
							name_kz:this.categories[i].word.kz,
							name_kz_desc:translate.commonWords.products.kz,
							name_ru:this.categories[i].word.ru ,
							name_ru_desc:translate.commonWords.products.ru,
							type:'product_id',
							current_kz:this.categories[i].word.kz.toLowerCase().indexOf(searchString)>-1
						});
						resultNodes.push({
							id:this.categories[i].id,
							name_kz:this.categories[i].word.kz,
							name_kz_desc:translate.commonWords.stores.kz,
							name_ru:this.categories[i].word.ru,
							name_ru_desc:translate.commonWords.stores.ru,
							type:'store_id',
							current_ru:this.categories[i].word.kz.toLowerCase().indexOf(searchString)>-1
						});
					}
					if(this.categories[i].children){
						let parents = {
							ru:this.categories[i].word.ru,
							kz:this.categories[i].word.kz
						};
						this.searchTreeNode(parents,this.categories[i].children,searchString,resultNodes);
					}    
				}
				return resultNodes;
            },
			searchTreeNode(parents,nodes,text,resultNodes){
				for(let i=0; i < nodes.length; i++){
					if(nodes[i].word.ru.toLowerCase().indexOf(text.toLowerCase())>-1||nodes[i].word.kz.toLowerCase().indexOf(text.toLowerCase())>-1){
						resultNodes.push({
							id:nodes[i].id,
							name_kz:nodes[i].word.kz,
							name_kz_desc:parents.kz+" | " + translate.commonWords.products.kz,
							name_ru:nodes[i].word.ru,
							name_ru_desc:parents.ru+" | " + translate.commonWords.products.ru,
							type:'product_id',
							current_kz:nodes[i].word.kz.toLowerCase().indexOf(text)>-1
						});
						resultNodes.push({
							id:nodes[i].id,
							name_kz:nodes[i].word.kz,
							name_kz_desc:parents.kz+" | " + translate.commonWords.stores.kz,
							name_ru:nodes[i].word.ru,
							name_ru_desc:parents.ru+" | " + translate.commonWords.stores.ru,
							type:'store_id',
							current_ru:nodes[i].word.kz.toLowerCase().indexOf(text)>-1
						});
					}
					if(nodes[i].children){
						let parents2 = {
							ru:parents.ru+' | '+this.categories[i].word.ru,
							kz:parents.kz+' | '+this.categories[i].word.kz
						};
						this.searchTreeNode(parents2,nodes[i].children,text,resultNodes);
					}
                }
			}

}