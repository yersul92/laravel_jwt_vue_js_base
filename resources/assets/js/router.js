import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', name:'home',component: require('./components/Home.vue') },//----------------------------------------- +
    { path: '/favourites', name:'favourites',component: require('./components/Favourites/Favourites.vue') },// ------- +
    { path: '/dashboard/:type', name:'dashboard',component: require('./components/Dashboard/Index.vue') },  // ------- +    
    { path: '/stores/:id', name:'stores',component: require('./components/Store/View.vue') },               // ------- +          
    { path: '/products/edit/:id', name:'productForm',component: require('./components/Product/Form.vue') },
    { path: '/stores/category/:id', name:'stores_by_cat',component: require('./components/Store/GroupByCat.vue') },//- +
    { path: '/auth/:type', name:'auth',component: require('./components/Auth/Auth.vue') },                         //- + 
    { path: '/search/query_text/:query_text', name:'search',component: require('./components/Search/Result.vue') } //- +
  ] 
  //mode: 'history' 
});

export default router