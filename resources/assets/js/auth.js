import Vue from 'vue';
import router from './router.js';

export default {
    user: {
        authenticated: false,
        profile: {
            days_left:0
        },
        roles:[],
        isAdmin:false,
        isAdsAdmin:false,
        isOperator:false,
        isStaff:false,
        firstAuth:false,
        name:null
    },
    check() {
        let token = localStorage.getItem('id_token');
        if (token !== null) {
            Vue.http.get(
                'api/user?token=' + token,
            ).then(response => {
                this.user.authenticated = true;
                this.user.profile = response.data.data.user;
                this.user.roles = response.data.data.roles;
                this.defineUserRole();
            }, response => {
                this.signout();
            })
        }
    },
    signin(context, phone, password) {
        context.signing = true;
        Vue.http.post(
            'api/signin',
            {
                phone: phone,
                password: password
            }
        ).then(response => {
            context.error = false;
            context.success = true;
            context.signing = false;
            localStorage.setItem('id_token', response.data.meta.token);
            Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');

            this.user.authenticated = true;
            this.user.name = response.data.data.name;
            this.user.roles = response.data.data.roles;
            this.defineUserRole();
            router.push({
                name: 'dashboard',
                params:{type:'profile'}
            })
        }, response => {
            context.error = true
            context.success = false;
            context.responseStatus = response.status;
            context.signing = false;
        })
    },
    signout() {
        localStorage.removeItem('id_token')
        this.user = {
            authenticated: false,
            profile: {
                days_left:0
            },
            roles:[],
            isAdmin:false,
            isAdsAdmin:false,
            isOperator:false,
            isStaff:false,
            firstAuth:false,
            name:null
        };
        router.push({
            name: 'home'
        })
    },
    defineUserRole(){
        for (var i = 0; i < this.user.roles.length; i++) {
            if(this.user.roles[i].name=='admin'){
                this.user.isAdmin = true;
            }
            if(this.user.roles[i].name=='admin'||this.user.roles[i].name=='ads_admin'){
                this.user.isAdsAdmin = true;
            }
            if(this.user.roles[i].name=='admin'||this.user.roles[i].name=='operator'){
                this.user.isOperator = true;
            }
        }
        this.user.isStaff = this.user.isAdmin == true||this.user.isAdsAdmin == true||this.user.isOperator == true;
    }    
}