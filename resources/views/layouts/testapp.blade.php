<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>xEditable and laravel 5. Inline and bulk editing examples.</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="dashboard_assets/css/bootstrap.min.css" />
    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="dashboard_assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body>
@yield('content')
<script src="dashboard_assets/js/jquery-2.1.4.min.js"></script>
<script src="dashboard_assets/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="dashboard_assets/css/bootstrap-editable.min.css" />
<script src="dashboard_assets/js/bootstrap-editable.min.js"></script>
@yield('scripts')
</body>
</html>
