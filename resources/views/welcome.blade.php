<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <title>Barbazar.kz</title>
    <meta name="description" content="barbazar.kz - бүкіл Қазақстанға арналған интернет сауда алаңы!">
    <meta name="description" content="barbazar.kz - интернет площадка для продавцов для всего Казахстана!">
    <meta name="mailru-domain" content="XYkpBHtV1K0CrRrx">
    <link href="css/app_17_01_2018.css" rel="stylesheet" type="text/css" media="all" /> 
    <link href="css/style_17_01_2018.css" rel="stylesheet" type="text/css" media="all" /> 
    <style>
       #app{
           margin-top:5px;
           margin-bottom:5px;
       }
    </style>
    <script src="https://widget.cloudpayments.kz/bundles/cloudpayments"></script>
</head>
<body>
    <div id="app"></div>
    <script src="js/app_17_01_2018_03.js"></script>
    <!--script src="js/popper.min.js"></script-->
</body>
</html>