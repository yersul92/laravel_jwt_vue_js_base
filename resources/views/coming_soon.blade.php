<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Barbazar.kz</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,300,100,100italic,300italic,400italic,700,700italic">
        
        <link rel="stylesheet" href="cs_assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="cs_assets/css/animate.css">
		<link rel="stylesheet" href="cs_assets/css/form-elements.css">
        <link rel="stylesheet" href="cs_assets/css/style.css">
        <link rel="stylesheet" href="cs_assets/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="https://unpkg.com/vue/dist/vue.min.js"></script>
		<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="cs_assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="cs_assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="cs_assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="cs_assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="cs_assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Coming Soon -->
        <div class="coming-soon">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                        	<div class="logo wow fadeInDown">
                        		<h1>
                        			<a href="">Erine - Bootstrap Coming Soon Template</a>
                        		</h1>
                        	</div>
                            <h2 class="wow fadeInLeftBig">Жөндеу жұмыстары жүргізілуде</h2>
                            <!--div class="timer wow fadeInUp">
                                <div class="days-wrapper">
                                    <span class="days"></span> <br>күн \ день 
                                </div> 
                                <span class="slash">/</span> 
                                <div class="hours-wrapper">
                                    <span class="hours"></span> <br>сағат \ час
                                </div> 
                                <span class="slash">/</span> 
                                <div class="minutes-wrapper">
                                    <span class="minutes"></span> <br>минут \ минут
                                </div> 
                                <span class="slash">/</span> 
                                <div class="seconds-wrapper">
                                    <span class="seconds"></span> <br>секунд \ секунд
                                </div>
                            </div-->
                            <div class="wow fadeInLeftBig">
                            	<p>
                            		Сайтта жөндеу жұмыстары жүргізілуде. Жақын арада жаңа нұсқасын күтіңіздер. 
                            		Проводятся работы по обновлению сайта. Скоро запустим сайт.
                            	</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
		
		<div id="app">
            <div  v-html="message"></div>
            <router-link :to="{name:'user',params:{id:89}}">89</router-link>			
			<router-view></router-view>
        </div>
	 
     	<script>
			const User = {
				template: '<div v-html="$route.params.id"></div>'
			}

			const router = new VueRouter({
			  routes: [
				{ name:'user',path: '/user/:id', component: User }
			  ]
			})
			var app = new Vue({ 
			    el: '#app' ,
				data:
				{
					message: "Hello world"
				},
				router
			});
			
		</script>
        <!-- Footer -->
        <footer>
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-7 footer-copyright">
	                    
	                </div>
	                <div class="col-sm-5 footer-social">
	                	<a class="social-icon vk" href="https://vk.com/barbazar.corporation" target="_blank"></a>
	                	<a class="social-icon google-plus" href="barbazar@gmail.com" target="_blank"></a>
                        <a class="social-icon instagram" href="http://instagram.com/barbazar_aktobe" target="_blank"></a>
	                </div>
	            </div>
	        </div>
        </footer>


        <!-- Javascript -->
        <script src="cs_assets/js/jquery-1.10.2.min.js"></script>
        <script src="cs_assets/js/jquery.backstretch.min.js"></script>
        <script src="cs_assets/js/jquery.countdown.min.js"></script>
        <script src="cs_assets/js/wow.min.js"></script>
        <script src="cs_assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="cs_assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
